//14:19:19.991 -> angles: Y:0.00  P:-3.32 R:-58.71
//


//////////////////////////////Include libraries//////////////////////////////////////////////////

#include <Servo.h>                  //Using Servo.h library to control ESC.
#include <Wire.h>                   //Using Wire.h library so we can communicate with the gyro.

///////////////////////////////////////////////////////////////////////////////////////////////
//////////pins 4 5 6 7 correspond to motor no. 1(CCW) 2(CW) 3(CCW) 4(CW) respectively
//////////p q r s represent motors 1 2 3 4 repectively
///////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

#define INTERRUPT_PIN 2  // use pin 2 on Arduino Uno & most boards
//#define LED_PIN 13 // (Arduino is 13, Teensy is 11, Teensy++ is 6

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

int t;
float correct;

MPU6050 mpu;
/////////////////////////////////////////////////////////////////////////////////////////////////

float pid_p_gain_roll = 1.5;               //Gain setting for the roll P-controller
float pid_i_gain_roll = 0.04;              //Gain setting for the roll I-controller
float pid_d_gain_roll = 18.0;              //Gain setting for the roll D-controller
int pid_max_roll = 400;                    //Maximum output of the PID-controller (+/-)

float pid_p_gain_pitch = pid_p_gain_roll;  //Gain setting for the pitch P-controller.
float pid_i_gain_pitch = pid_i_gain_roll;  //Gain setting for the pitch I-controller.
float pid_d_gain_pitch = pid_d_gain_roll;  //Gain setting for the pitch D-controller.
int pid_max_pitch = pid_max_roll;          //Maximum output of the PID-controller (+/-)

float pid_p_gain_yaw = 4.0;                //Gain setting for the pitch P-controller. //4.0
float pid_i_gain_yaw = 0.02;               //Gain setting for the pitch I-controller. //0.02
float pid_d_gain_yaw = 0.0;                //Gain setting for the pitch D-controller.
int pid_max_yaw = 400;                     //Maximum output of the PID-controller (+/-)

boolean auto_level = true;                 //Auto level on (true) or off (false)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
char data1, data2;                         //Variable for storing received data from HC 05
float p1 = 0, q1 = 0;
float r1 = 0, s1 = 0;                   //Variables for motors
int throttle_ = 0, thro_map = 0;     //Variable for mapping throttle and standardised throttle variables
int val1, val2, val3, val4;         //Creating 'val' variables for mapping motors
int esc_1, esc_2, esc_3, esc_4;     //Variables for ESC pulses in active control
int flag = 0;

Servo esc1;                         //Creating a servo class with name as esc for all motors
Servo esc2;
Servo esc3;
Servo esc4;

float y, p, r;
float roll_level_adjust, pitch_level_adjust;



// program state
enum State { STOPPED, STARTING, STARTED, INITIAL };
State state = STOPPED;



unsigned long loop_timer = 0;        // loop timer
unsigned long test_timer = 0;        // test timer

//struct Gyroscope gyro;              // gyroscope
//struct PID pid;                     // PID settings

double pitch_angle, roll_angle, yaw_angle;
double pitch_acc = 0, roll_acc = 0;
//double landing_throttle;

long acc_x, acc_y, acc_z, acc_total_vector;
unsigned long timer_channel_1, timer_channel_2, timer_channel_3, timer_channel_4, esc_timer, esc_loop_timer;
unsigned long timer_1, timer_2, timer_3, timer_4, current_time;
//unsigned long loop_timer, test_timer;
double gyro_pitch, gyro_roll, gyro_yaw;
double gyro_axis_cal[4];
float pid_error_temp;
float pid_i_mem_roll, pid_roll_setpoint, gyro_roll_input = 0, pid_output_roll, pid_last_roll_d_error;
float pid_i_mem_pitch, pid_pitch_setpoint, gyro_pitch_input = 0, pid_output_pitch, pid_last_pitch_d_error;
float pid_i_mem_yaw, pid_yaw_setpoint, gyro_yaw_input = 0, pid_output_yaw, pid_last_yaw_d_error;
float angle_roll_acc, angle_pitch_acc, angle_pitch, angle_roll;
boolean gyro_angles_set;

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
} 

void setup() {
//////////////////////////////////////////////

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);   // turn the onboard LED on

  Serial.begin(9600);

    Wire.begin();
    Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties

esc1.attach(5);                            //Specify the esc signal pins
esc2.attach(6);
esc3.attach(10);
esc4.attach(11);
esc1.writeMicroseconds(1000);              //initialize the signal to 1000
esc2.writeMicroseconds(1000);
esc3.writeMicroseconds(1000);
esc4.writeMicroseconds(1000);

    // initialize device
    Serial.println(F("Initializing I2C devices..."));
    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    // verify connection
    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();
    

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
        Serial.print(digitalPinToInterrupt(INTERRUPT_PIN));
        Serial.println(F(")..."));
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
        } 
        else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }

        mpu.setXGyroOffset(111);
        mpu.setYGyroOffset(-62);
        mpu.setZGyroOffset(0);
        mpu.setZAccelOffset(1214); 

/////////////////////////////////////////////
  
  // zero data structures
//  memset(&gyro, 0, sizeof(gyro));
//  memset(&pid, 0, sizeof(pid)); 

//Wire.begin();                              //Start the I2C as master.


  // start the gyroscope
//  gyro.address = GYRO_ADDR;
//  enableGyro(&gyro);

  // read calibration values from gyroscope
/*  while (!readGyroFromEEPROM(gyro, GYRO_STRUCT_LOC)) {
    delayMicroseconds(250);
  }
*/
  // setup PID settings
//  setupPID(&pid);

Serial.println(F("Connect BT controller"));
Serial.println(F("Press SELECT to proceed to loop"));
  check_to_continue();

//  loop_timer = micros();

  //When setup() is done, turn off the onboard led.
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
////////////////////////////////////////////////////////////////////////////////////////////////
     while (!mpuInterrupt && fifoCount < packetSize) {
        if (mpuInterrupt && fifoCount < packetSize) {
          // try to get out of the infinite loop 
          fifoCount = mpu.getFIFOCount();
        }
     }  


    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        fifoCount = mpu.getFIFOCount();
       // Serial.println(F("FIFO overflow: "));  Serial.println(fifoCount);
        delay(10);
    }
    
    // otherwise, check for DMP data ready interrupt (this should happen frequently)
     else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
     }

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

         // display Euler angles in degrees
            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetAccel(&aa, fifoBuffer);
            mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
            mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);

            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
                        
            if (t <= 300) {
              correct = ypr[0];
              t++;
            }

            ypr[0] = ypr[0] - correct;
         /*  
            Serial.print("euler from DMP\t");
            Serial.print(ypr[0] * 180/M_PI);
            Serial.print("\t");
            Serial.print("\t");
            Serial.print("\t"); */

            y = ypr[0] * 180/M_PI;
            p = ypr[1] * 180/M_PI;
            r = ypr[2] * 180/M_PI;

            gyro_yaw_input = y;
            gyro_pitch_input = p;
            gyro_roll_input = r;
            
///////////////////////////////////////////////////////////////////////////////////////            
  
///////////////////////////////////////////////////////////////////////////////////////
////////////////////////Bluetooth Control (Passive)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

  if(Serial.available() > 0){
   data1 = Serial.read();
   if(data1 == 'U'){        //U = Up (Coarse)
    p1++;
    q1++;
    r1++;
    s1++;
    thro_map++;
   }
   if(data1 == 'D'){        //D = Down (Coarse)
    p1--;
    q1--;
    r1--;
    s1--;
    thro_map--;
   }
   if(data1 == 'u'){        //u = Up  (Fine)
    p1 += 0.5;
    q1 += 0.5;
    r1 += 0.5;
    s1 += 0.5;
    thro_map += 0.5;
   }
   if(data1 == 'd'){        //D = Down  (Fine)
    p1 -= 0.5;
    q1 -= 0.5;
    r1 -= 0.5;
    s1 -= 0.5;
    thro_map -= 0.5;
   }
   
 /*  if(data1 == 'F'){        //F = Front
    q1++;
    r1++;}
   if(data1 == 'B'){        //B = Back
    p1++;
    s1++;}
   if(data1 == 'L'){        //L = Left
    p1++;
    q1++;}
   if(data1 == 'R'){        //R = Right
    r1++;
    s1++;}*/
   if(data1 == 'E'){        //E = Emergency stop
    p1 = 0;
    q1 = 0;
    r1 = 0;
    s1 = 0;
    thro_map = 0;
    setState(STOPPED);
    resetESCPulses();
 // while(micros() - loop_timer < 4000);                        //Wait until 4000us are passed.
 // loop_timer = micros();                                      //Set the timer for the next loop.
    esc1.writeMicroseconds(esc_1);                              //giving the emergency stop signal to esc
    esc2.writeMicroseconds(esc_2);
    esc3.writeMicroseconds(esc_3); 
    esc4.writeMicroseconds(esc_4);
   }
}

throttle_ = 1000 + (thro_map*100);    //Standardising throttle variable

   if(thro_map > 10){
   p1 = 10;
   q1 = 10;
   r1 = 10;
   s1 = 10;
   thro_map = 10;}
   if(thro_map < 0){
   p1 = 0;
   q1 = 0;
   r1 = 0;
   s1 = 0;
   thro_map = 0;}

val1 = 1000 + (p1*100);                                        //mapping motor throttle to match ESC pulse range
val2 = 1000 + (q1*100);
val3 = 1000 + (r1*100);
val4 = 1000 + (s1*100);
/*
val1 = map(val1, 0, 10,1000,2000);  //mapping motor1 throttle to minimum and maximum(Change if needed) 
val2 = map(val2, 0, 10,1000,2000);  //mapping motor2 throttle to minimum and maximum(Change if needed) 
val3 = map(val3, 0, 10,1000,2000);  //mapping motor3 throttle to minimum and maximum(Change if needed) 
val4 = map(val4, 0, 10,1000,2000);  //mapping motor4 throttle to minimum and maximum(Change if needed) 


esc1.writeMicroseconds(val1);       //giving the signal to esc
esc2.writeMicroseconds(val2);
esc3.writeMicroseconds(val3); 
esc4.writeMicroseconds(val4);
*/
/*
if(data1 == 'F'){
    delay(300);
    q--;
    r--;
    val2 = q;
    val3 = r;
    val2 = map(val2, 0, 10,1000,2000); 
    val3 = map(val3, 0, 10,1000,2000);
    esc2.writeMicroseconds(val2);
    esc3.writeMicroseconds(val3);
   }
if(data1 == 'B'){
    delay(300);
    p1--;
    s1--;
    val1 = p1;
    val4 = s1;
    val1 = map(val1, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc4.writeMicroseconds(val4);
   }
if(data1 == 'L'){
    delay(300);
    p1--;
    q1--;
    val1 = p1;
    val2 = q1;
    val1 = map(val1, 0, 10,1000,2000); 
    val2 = map(val2, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc2.writeMicroseconds(val2);
    }
if(data1 == 'R'){
    delay(300);
    r1--;
    s1--;
    val3 = r; 
    val4 = s;
    val3 = map(val3, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc3.writeMicroseconds(val3); 
    esc4.writeMicroseconds(val4);
    }*/
data1='Z';

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Active Control///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

    Serial.print("angles: Y:");
    Serial.print(y);Serial.print("\tP:");
    Serial.print(p);Serial.print("\tR:");
    Serial.print(r);Serial.print("\t:");
  

 if(!auto_level){                                                          //If the quadcopter is not in auto-level mode
    pitch_level_adjust = 0;                                                 //Set the pitch angle correction to zero.
    roll_level_adjust = 0;                                                  //Set the roll angle correcion to zero.
  }

  pid_roll_setpoint = 0;

  pid_pitch_setpoint = 0;

  pid_yaw_setpoint = 0;

  calculate_pid();                                                         //PID inputs are known. So we can calculate the pid output.

////////////// calculating ESC pulses necessary to level/////////////////////////////

  // calculate current orientation according to the gyro
  // use a 70-30 complementary filter
  // 65.5 is from the MPU-6050 spec

//  readGyroValues(&gyro);                    //read values from gyro
  
  
/*  Serial.print("gyro values are: ");
    Serial.print(gyro.yaw);Serial.print("\t");
    Serial.print(gyro.roll);Serial.print("\t");
    Serial.print(gyro.pitch);Serial.print("\t");
*/


//    Serial.print("\t pid YRP values:  ");
//    Serial.print(pid.yaw.gyro);Serial.print("\t");
//    Serial.print(pid.roll.gyro);Serial.print("\t");
//    Serial.print(pid.pitch.gyro);Serial.print("\t");

  // calculate the angles
/*  // 0.0000611 = 1 / (250Hz / 65.5)
  pitch_angle += gyro.pitch * 0.0000611;
  roll_angle += gyro.roll * 0.0000611;
  yaw_angle = gyro.yaw * 0.0000611;
  
  // shift the roll/pitch axes if the drone has yawed
  pitch_angle -= roll_angle * sin(gyro.yaw * 0.000001066);
  roll_angle += pitch_angle * sin(gyro.yaw * 0.000001066);
*/

  
/*  // calculate the total acceleration
  double total_acc = sqrt(pow2(gyro.acc.x) + pow2(gyro.acc.y) + pow2(gyro.acc.z));
  if (abs(gyro.acc.y) < total_acc)
    pitch_acc = asin((float)gyro.acc.y/total_acc) * 57.296;
  if (abs(gyro.acc.x) < total_acc)
    roll_acc = -asin((float)gyro.acc.x/total_acc) * 57.296;

roll_angle -= 0.0; 
pitch_angle -= 0.0; 

  // use acceleration to correct the drift from the gyro
  roll_angle = (roll_angle * 0.9996) + (roll_acc * 0.0004);
  pitch_angle = (pitch_angle * 0.9996) + (pitch_acc * 0.0004);

//    Serial.print("\t Angles are: ");
//    Serial.print(roll_angle);Serial.print("\t");
//    Serial.print(pitch_angle);Serial.print("\t");
*/
//  double roll_adjust = r * 16.0;
//  double pitch_adjust = p * 16.0;
  
  // based on program state, do different things
  switch (state) {
  case STOPPED: {
    Serial.println("In STOPPED MODE");
    if (throttle_ >= 1200){setState(STARTING);
    Serial.println("switched to STARTING MODE");
    }
    resetESCPulses();
    if (flag == 1){
  //  while(micros() - loop_timer < 4000);                                      //We wait until 4000us are passed.
  //  loop_timer = micros();                                                    //Set the timer for the next loop.

    esc1.writeMicroseconds(esc_1);       //giving the stop signal to esc
    esc2.writeMicroseconds(esc_2);
    esc3.writeMicroseconds(esc_3); 
    esc4.writeMicroseconds(esc_4);
    flag = 0;
    }
    break;
   }

  case STARTING: {

    //Reset the PID controllers for a bumpless start.
    pid_i_mem_roll = 0;
    pid_last_roll_d_error = 0;
    pid_i_mem_pitch = 0;
    pid_last_pitch_d_error = 0;
    pid_i_mem_yaw = 0;
    pid_last_yaw_d_error = 0;
    
    setState(INITIAL);
    Serial.println("switched to INITIAL MODE");
    resetESCPulses();
    test_timer = micros();
    break;
    }
  
  case INITIAL: {
    //Serial.println("In INITIAL MODE");
if (throttle_ < 1200){setState(STOPPED);flag = 1;
//    Serial.println("switched to STOPPED MODE");
    }
    
    if (throttle_ > 1800) throttle_ = 1800;                                   //We need some room to keep full control at full throttle.
    esc_1 = throttle_ - pid_output_pitch + pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 1 (front-right - CCW)
    esc_2 = throttle_ + pid_output_pitch + pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 2 (rear-right - CW)
    esc_3 = throttle_ + pid_output_pitch - pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 3 (rear-left - CCW)
    esc_4 = throttle_ - pid_output_pitch - pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 4 (front-left - CW)

    if (esc_1 < 1100) esc_1 = 1100;                                         //Keep the motors running.
    if (esc_2 < 1100) esc_2 = 1100;                                         //Keep the motors running.
    if (esc_3 < 1100) esc_3 = 1100;                                         //Keep the motors running.
    if (esc_4 < 1100) esc_4 = 1100;                                         //Keep the motors running.

    if (esc_1 > 1400)esc_1 = 1400;
    if (esc_2 > 1400)esc_2 = 1400;
    if (esc_3 > 1400)esc_3 = 1400;
    if (esc_4 > 1400)esc_4 = 1400; 
    
    Serial.print("Initial ESC pulses are:  ");
    Serial.print(esc_1);Serial.print("\t");
    Serial.print(esc_2);Serial.print("\t");
    Serial.print(esc_3);Serial.print("\t");
    Serial.println(esc_4);//Serial.print("\t");

    esc1.writeMicroseconds(esc_1);                                                  //giving the signal to ESCs
    esc2.writeMicroseconds(esc_2);
    esc3.writeMicroseconds(esc_3);
    esc4.writeMicroseconds(esc_4);

if ((micros() - test_timer )> 15000000){setState(STARTED);
//    Serial.println("switched to STARTED MODE");
    }
    
    break;
  }

  case STARTED: {
//    Serial.println("In STARTED MODE");
    if (throttle_ < 1200){setState(STOPPED);flag = 1;
//    Serial.println("switched to STOPPED MODE");
    }

    if (throttle_ > 1800) throttle_ = 1800;                                   //We need some room to keep full control at full throttle.
    esc_1 = throttle_ - pid_output_pitch + pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 1 (front-right - CCW)
    esc_2 = throttle_ + pid_output_pitch + pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 2 (rear-right - CW)
    esc_3 = throttle_ + pid_output_pitch - pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 3 (rear-left - CCW)
    esc_4 = throttle_ - pid_output_pitch - pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 4 (front-left - CW)

    if (esc_1 < 1100) esc_1 = 1100;                                         //Keep the motors running.
    if (esc_2 < 1100) esc_2 = 1100;                                         //Keep the motors running.
    if (esc_3 < 1100) esc_3 = 1100;                                         //Keep the motors running.
    if (esc_4 < 1100) esc_4 = 1100;                                         //Keep the motors running.

    if (esc_1 > 2000)esc_1 = 2000;
    if (esc_2 > 2000)esc_2 = 2000;
    if (esc_3 > 2000)esc_3 = 2000;
    if (esc_4 > 2000)esc_4 = 2000;

    Serial.print("Running ESC pulses are:  ");
    Serial.print(esc_1);Serial.print("\t");
    Serial.print(esc_2);Serial.print("\t");
    Serial.print(esc_3);Serial.print("\t");
    Serial.println(esc_4);//Serial.print("\t");

    esc1.writeMicroseconds(esc_1);                                                  //giving the signal to ESCs
    esc2.writeMicroseconds(esc_2);
    esc3.writeMicroseconds(esc_3);
    esc4.writeMicroseconds(esc_4);

//  Serial.println(esc_4);//Serial.print("\t");
//  readGyroValues(&gyro);
    
    break;
  }
 }
}

void calculate_pid(){
  //Roll calculations
  pid_error_temp = gyro_roll_input - pid_roll_setpoint;
  pid_i_mem_roll += pid_i_gain_roll * pid_error_temp;
  if(pid_i_mem_roll > pid_max_roll)pid_i_mem_roll = pid_max_roll;
  else if(pid_i_mem_roll < (pid_max_roll * -1))pid_i_mem_roll = pid_max_roll * -1;

  pid_output_roll = pid_p_gain_roll * pid_error_temp + pid_i_mem_roll + pid_d_gain_roll * (pid_error_temp - pid_last_roll_d_error);
  if(pid_output_roll > pid_max_roll)pid_output_roll = pid_max_roll;
  else if(pid_output_roll < (pid_max_roll * -1))pid_output_roll = pid_max_roll * -1;

  pid_last_roll_d_error = pid_error_temp;

  //Pitch calculations
  pid_error_temp = gyro_pitch_input - pid_pitch_setpoint;
  pid_i_mem_pitch += pid_i_gain_pitch * pid_error_temp;
  if(pid_i_mem_pitch > pid_max_pitch)pid_i_mem_pitch = pid_max_pitch;
  else if(pid_i_mem_pitch < (pid_max_pitch * -1))pid_i_mem_pitch = pid_max_pitch * -1;

  pid_output_pitch = pid_p_gain_pitch * pid_error_temp + pid_i_mem_pitch + pid_d_gain_pitch * (pid_error_temp - pid_last_pitch_d_error);
  if(pid_output_pitch > pid_max_pitch)pid_output_pitch = pid_max_pitch;
  else if(pid_output_pitch < (pid_max_pitch * -1))pid_output_pitch = pid_max_pitch * -1;

  pid_last_pitch_d_error = pid_error_temp;

  //Yaw calculations
  pid_error_temp = gyro_yaw_input - pid_yaw_setpoint;
  pid_i_mem_yaw += pid_i_gain_yaw * pid_error_temp;
  if(pid_i_mem_yaw > pid_max_yaw)pid_i_mem_yaw = pid_max_yaw;
  else if(pid_i_mem_yaw < (pid_max_yaw * -1))pid_i_mem_yaw = pid_max_yaw * -1;

  pid_output_yaw = pid_p_gain_yaw * pid_error_temp + pid_i_mem_yaw + pid_d_gain_yaw * (pid_error_temp - pid_last_yaw_d_error);
  if(pid_output_yaw > pid_max_yaw)pid_output_yaw = pid_max_yaw;
  else if(pid_output_yaw < (pid_max_yaw * -1))pid_output_yaw = pid_max_yaw * -1;

  pid_last_yaw_d_error = pid_error_temp;
}

inline void setState(State s) {
  state = s;}

inline void resetESCPulses() {
  esc_1 = 1000;
  esc_2 = 1000;
  esc_3 = 1000;
  esc_4 = 1000;}
  
void check_to_continue(){
  byte continue_byte = 0;
  while(continue_byte == 0){
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off
  delay(300);                       // wait for 300 ms
  digitalWrite(LED_BUILTIN, HIGH);    // turn the LED on
  delay(300);                       // wait for 300 ms
  if(Serial.available() > 0){
   data2 = Serial.read();
   if(data2 == 'C')continue_byte = 1;}}
  delay(100);
  data2 = 'Z';
}
